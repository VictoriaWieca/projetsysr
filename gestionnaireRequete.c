//traite les requetes des clients
#include <sys/types.h>
#include <sys/ipc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/shm.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "messages.h"
#define PORT 5000

#define CLE 217  //clé de la SHM
#define CLE2 218

int msgid;
char* envoyerGestionnaireCompte(char* msg);
void fin(int n) {
	fprintf(stderr, "Terminaison du serveur.\n");
	msgctl(msgid, IPC_RMID, NULL);
	exit(EXIT_SUCCESS);
}

char* demandeConnexion(char* message){
	char* tmp = (char*)malloc(256*sizeof(char));
	strcpy(tmp,envoyerGestionnaireCompte(message));
	if(strstr(tmp,"oui") != NULL){
		return "Réussite";
	}
	else {
		return "Echec";
	}
}
 
char* demandeDeconnexion(){

	//memoire();//avec un param a delete
	printf("déconnecté\n");
}

char* demandeInscription(char* message){
	//envoyerGestionnaireCompte(message);
	//si ok alors on l'ajoute au fichier de gestion comptes
	//sinon msg erreur
	char* tmp = (char*)malloc(256*sizeof(char));
	strcpy(tmp,envoyerGestionnaireCompte(message));
	if(strstr(tmp,"oui") != NULL){
		return "Vous êtes inscrit !";
	}
	else {
		return "Ce pseudo existe déjà";
	}
}

char* suppCompte(char* message){
	//envoyerGestionnaireCompte(message);
	//retire de la liste des utilisateurs
	printf("compte supp\n");
	//demandeDeconnexion();
}

void liste(){
	//memoire();
	printf("la liste\n");
}


void recupRequete(){
    key_t cle;
	struct sigaction action;
	requete req;
	reponse rep;
	int sig, i, tmp;
	
	// on commence par prevoir la terminaison sur signal du serveur
	action.sa_handler = fin;
	for(i=1; i<NSIG; i++) sigaction(i, &action, NULL);	// installation du handler de fin pour tous les signaux
	// creation de la file de message
	if ((cle = ftok(SERVEUR, '0')) == -1) {
		fprintf(stderr, "Obtention de la cle impossible. Fin du serveur.\n");
		exit(EXIT_FAILURE);
	}
	if ((msgid = msgget(cle, IPC_CREAT|IPC_EXCL|DROITS)) == -1) {
		fprintf(stderr, "Creation de la file impossible. Fin du serveur.\n");
		exit(EXIT_FAILURE);
	}
	// attente d'une requete a l'infini...
	while(1) {
		if (msgrcv(msgid, &req, TAILLE_REQ, 1L, 0) == -1) {
			fprintf(stderr, "Serveur: Erreur de reception de requete\n");
			continue;
		}
		// construction de la reponse
		rep.type = req.signature;
		tmp = strlen(req.chaine);
		if (strstr(req.chaine, ">connexion") !=  NULL) {
			strcpy(rep.chaine,demandeConnexion(req.chaine));
		}
		if (strstr(req.chaine, ">deconnexion") !=  NULL) {
			demandeDeconnexion();
		}
		if (strstr(req.chaine, ">inscription") !=  NULL) {
			strcpy(rep.chaine,demandeInscription(req.chaine));
		}
		if (strstr(req.chaine, ">desinscription") !=  NULL) {
			suppCompte(req.chaine);
		}
		if (strstr(req.chaine, ">liste") !=  NULL) {
			liste();
		}
		if (strstr(req.chaine, ">quitter") != NULL) {
			printf("Au revoir !\n");
			exit(EXIT_SUCCESS);
		}
		else{

		}
		printf("rep.chaine : %s\n",rep.chaine);
		
		printf("Serveur: rep.chaine ->%s<-\n", rep.chaine);
		// envoi de la reponse
		if (msgsnd(msgid, &rep, TAILLE_REP, 0) == -1) {
			fprintf(stderr, "Serveur: Erreur d'envoi d'une réponse a %d\n", req.signature);
		}
		else {
			printf("Serveur: reponse envoyee a %d\n", req.signature);
		}
	}
}

char* envoyerGestionnaireCompte(char* msg){
    int sockfd;
    char buffer[20];
    struct sockaddr_in     servaddr;  
    // Creating socket file descriptor
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }  
    memset(&servaddr, 0, sizeof(servaddr));      
    // Filling server information
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    servaddr.sin_addr.s_addr = INADDR_ANY;      
    int n, len;     
    sendto(sockfd, (const char *)msg, strlen(msg),
        MSG_CONFIRM, (const struct sockaddr *) &servaddr, 
            sizeof(servaddr));
    printf("message envoyé : %s\n", msg);
           
    n = recvfrom(sockfd, (char *)buffer, 256, 
                MSG_WAITALL, (struct sockaddr *) &servaddr,
                &len);
    buffer[n] = '\0';
	char* tmp = (char*)malloc(256*sizeof(char));
	strcpy(tmp,buffer);
    close(sockfd);
	return tmp;
}

void memoire(){
	//récuperer les connectés et leur nombre
	int nbConnectes=3;
	char* tabConnectes[3];
	tabConnectes[0]="hello";
	tabConnectes[1]="hi";
	tabConnectes[2]="salut";
	char* mem[nbConnectes]; //pointeur sur la SHM
	int shmid; //id de la SHM
	shmid = shmget((key_t)CLE, 0, 0); //recup id SHM
	for(int i=0;i<nbConnectes;i++){
		mem[i]=shmat(shmid+i,NULL,0);
		//strcpy(mem[i],tabConnectes[i]); //attachement SHM
		//shmdt(&mem[i]);
	}
	for(int i = 0; i<3; i++){
		printf("message envoyé : %s\n", mem[i]);
	}
	
	exit(0);
}

int main(){
	recupRequete();
}