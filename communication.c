//recupere requetes des clients (envoie à gestion requete) et fait suivre les messages à tous les clients actuellement connectes
#include"communication.h"
#define PORT 2058
#define MAX 256
#define SA struct sockaddr


void recupererMessageClient(){
	int sockfd, connfd, len;
	struct sockaddr_in servaddr, cli;

	// socket create and verification
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1) {
		printf("socket creation failed...\n");
		exit(0);
	}
	else
		printf("Socket successfully created..\n");
	bzero(&servaddr, sizeof(servaddr));

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(PORT);

	if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) {
		printf("socket bind failed...\n");
		exit(0);
	}
	else
		printf("Socket successfully binded..\n");

	if ((listen(sockfd, 5)) != 0) {
		printf("Listen failed...\n");
		exit(0);
	}
	else
		printf("Server listening..\n");
	len = sizeof(cli);

	// Accept the data packet from client and verification
	connfd = accept(sockfd, (SA*)&cli, &len);
	if (connfd < 0) {
		printf("server accept failed...\n");
		exit(0);
	}
	else
		printf("server accept the client...\n");

	char buff[MAX];
	int n;
	for (;;) {
		bzero(buff, MAX);

		read(connfd, buff, MAX);
		char* tmp=(char*)malloc(256*sizeof(char));
		strcpy(tmp,envoyerRequete(buff));
		bzero(buff, MAX);
		n = 0;
		strcpy(buff,tmp);
		write(connfd, buff, MAX);
	}

	close(sockfd);
}


void envoyerTube(char* msg){
    
    int d_writer = open("myTube", O_WRONLY);
    printf("Writing to pipe: '%s'\n", msg);
    write(d_writer, msg, MAX);
    close(d_writer);
}



char* envoyerRequete(char* message){
    key_t cle;
	int msgid, i;
	requete req;
	reponse rep;
	// On recherche la file de message
	if ((cle = ftok(SERVEUR, '0')) == -1) {
		fprintf(stderr, "Obtention de la cle impossible. Fin du serveur.\n");
		exit(EXIT_FAILURE);
	}
	if ((msgid = msgget(cle, 0)) == -1) {
		fprintf(stderr, "Erreur: file inconnue. Fin du client.\n");
		exit(EXIT_FAILURE);
	}
	//
	req.type = 1L;
	req.signature = getpid();
	while(1) {
	    printf("--> ");
        strcpy(req.chaine, message);
        printf("req.chaine : %s\n", req.chaine);
		req.chaine[strlen(req.chaine)-1]='\0';
		if (strlen(req.chaine) == 0) continue;
        char c = req.chaine[0];
		if (c == '>') {
            if (strstr(req.chaine, ">quitter") !=  NULL) {
                printf("Au revoir !\n");
                exit(EXIT_SUCCESS);
            }
            //alors on envoie la requete
            printf("Requete de %d envoyee: ->%s<-\n", req.signature, req.chaine);
            if (msgsnd(msgid, &req, TAILLE_REQ, 0) == -1) {
                fprintf(stderr, "%d: erreur d'envoi de la requete %s au serveur\n", req.signature, req.chaine);
                fprintf(stderr, "Fin du client %d.\n", req.signature);
                exit(EXIT_FAILURE);
            }
            if (msgrcv(msgid, &rep, TAILLE_REP, (long) req.signature, 0) == -1) {
                fprintf(stderr, "%d: erreur de reception de la reponse du serveur.\n", req.signature);
                fprintf(stderr, "Fin du client %d.\n", req.signature);
                exit(EXIT_FAILURE);
            }
        }
        else{
           envoyerTube(message);
        }
        printf("%d: reponse du serveur ->%s<-\n", req.signature, rep.chaine);
        char* tmp=(char*)malloc(256*sizeof(char));
        strcpy(tmp,rep.chaine);
        return tmp;	
    }
}



