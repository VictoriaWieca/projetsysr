//communique avec le gestionnaire de requêtes via un socket UDP

#include <sys/types.h>
#include <sys/ipc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/shm.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#define PORT 5000
#define EXIT_SUCCESS 0

char* recupPseudoMsg(char* msg){
    int i=0;
    char *str=(char*)malloc(strlen(msg)*sizeof(char));
    strcpy(str,msg);
	int init_size = strlen(str);
	char delim[] = ",";

	char *ptr = strtok(str, delim);
    char* tab[2];
    for(int i=0;i<2;i++){
        tab[i]=(char*)malloc(256*sizeof(char));
    }

	while(ptr != NULL)
	{
        strcpy(tab[i],ptr);
		ptr = strtok(NULL, delim);
        i++;
	}
    return tab[0];
}

char* enleverRequete(char* message){
int i=0;
char *str=(char*)malloc(strlen(message)*sizeof(char));
strcpy(str,message);
int init_size = strlen(str);
char delim[] = ",";
char *ptr = strtok(str, delim);
char* tab[3];
for(int i=0;i<3;i++){
tab[i]=(char*)malloc(256*sizeof(char));
}
while(ptr != NULL)
{
strcpy(tab[i],ptr);
ptr = strtok(NULL, delim);
i++;
}

char* s=strcat(tab[1],tab[2]);
return s;
}

short verifierConnexion(char* msg){
    FILE* fd;
	char c;
    int found=0;
    int pos=0;
    int length;
    fd = fopen("identifiants.txt", "r+");
    if (!fd) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    length=strlen(msg);

    while( fread(&c,1,1,fd)>0 && !found ) {

    if( c==msg[pos] ) {
      pos++;
    } else {
      if(pos!=0) {
        fseek(fd,-pos,SEEK_CUR);
        pos=0;
      }
    }

    found = (pos==length);
    
    
   }
   fclose(fd);
   return found;
  }

short verifierInscription(char* msg){
    FILE* fd;
	char c;
    int found=0;
    int pos=0;
    int length;
    //char* strSearch="pseudo,mdp";
    fd = fopen("identifiants.txt", "r+a");
    if (!fd) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    char* pseudo=(char*)malloc(strlen(msg)*sizeof(char));
    strcpy(pseudo,recupPseudoMsg(msg));
    length=strlen(pseudo);

    while( fread(&c,1,1,fd)>0 && !found ) {

    if( c==pseudo[pos] ) {
      pos++;
    } else {
      if(pos!=0) {
        fseek(fd,-pos,SEEK_CUR);
        pos=0;
      }
    }

    found = (pos==length);
    
    
   }
   if(!found){
       fprintf(fd,"%s",msg);

   }
   fclose(fd);
   return !found;

}


void recupererGestionnaireRequete(){
    printf("etape 1");
    int sockfd;
    char buffer[20];
    struct sockaddr_in servaddr, cliaddr; 
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }
    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));
    servaddr.sin_family = AF_INET; // IPv4
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(PORT); 
    if ( bind(sockfd, (const struct sockaddr *)&servaddr, 
        sizeof(servaddr)) < 0 )
    {
    perror("bind failed");
    exit(EXIT_FAILURE);
    }
    int len, n;
    while(1){
        len = sizeof(cliaddr); 
        n = recvfrom(sockfd, (char *)buffer, 256, 
        MSG_WAITALL, ( struct sockaddr *) &cliaddr,
        &len);
        buffer[n] = '\0';
        if (strstr(buffer, ">connexion") !=  NULL) {
			if(verifierConnexion(enleverRequete(buffer))){
                sendto(sockfd,"oui", strlen("oui"), MSG_CONFIRM, (const struct sockaddr *) &cliaddr,len);
            }
            else{
                sendto(sockfd,"non", strlen("oui"), MSG_CONFIRM, (const struct sockaddr *) &cliaddr,len);
            }
		}
		if (strstr(buffer, ">inscription") !=  NULL) {
			if(verifierInscription(enleverRequete(buffer))){
                sendto(sockfd,"oui", strlen("oui"), MSG_CONFIRM, (const struct sockaddr *) &cliaddr,len);
            }
            else{
                sendto(sockfd,"non", strlen("oui"), MSG_CONFIRM, (const struct sockaddr *) &cliaddr,len);
            }
		}
    }
}
int main(){
    recupererGestionnaireRequete();
    
}