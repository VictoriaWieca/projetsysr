//communique avec processus communication de partieCentralisee avec sockets TCP
//communique avec l'afficheur client avec tube nommé
#include <sys/types.h>
#include <sys/ipc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/shm.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "messages.h"
#define MAX 256
#define PORT 2058
#define SA struct sockaddr
#define EXIT_SUCCESS 0


char* structToString(connexion co);

void communiquer(){
	int sockfd, connfd;
	struct sockaddr_in servaddr, cli;

	// socket create and verification
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1) {
		printf("socket creation failed...\n");
		exit(0);
	}
	else
		printf("Socket successfully created..\n");
	bzero(&servaddr, sizeof(servaddr));

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(PORT);

	if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) {
		printf("connection with the server failed...\n");
		exit(0);
	}
	else
		printf("connected to the server..\n");

	char texte[MAX];
	int n;
	for (;;) {
		bzero(texte, sizeof(texte));
		printf("Enter the string : ");
		n = 0;
        fgets(texte,256,stdin);
		char c = texte[0];
		if (c == '>') {
		    if (strstr(texte, ">connexion")!=NULL) {
		        connexion co;
		        char pseudo[20];
		        char mdp[20];
		        strcpy(co.requetes,texte);

		        printf("pseudo : \n");
		        fgets(pseudo, 20, stdin);
		        strcpy(co.pseudo,pseudo);

		        printf("mot de passe : \n");
		        fgets(mdp, 20, stdin);
		        strcpy(co.mdp,mdp);
		        write(sockfd,structToString(co), MAX);
		    }
		    if (strstr(texte, ">deconnexion") !=NULL) {
		        write(sockfd, texte, sizeof(texte));
		    }
		    if (strstr(texte, ">inscription") !=NULL) {
		        connexion inscr;
		        char pseudo[20];
		        char mdp[20];
		        strcpy(inscr.requetes,texte);

		        printf("pseudo : \n");
		        fgets(pseudo, 20, stdin);
		        strcpy(inscr.pseudo,pseudo);

		        printf("mot de passe : \n");
		        fgets(mdp, 20, stdin);
		        strcpy(inscr.mdp,mdp);
		        write(sockfd,structToString(inscr),MAX);
		    }
		    if (strstr(texte, ">desinscription") !=NULL) {
		        write(sockfd, texte, MAX);
		    }
		    if (strstr(texte, ">liste") !=NULL) {
		        write(sockfd, texte, MAX);
		    }
		    if (strstr(texte, ">quitter") !=NULL) {
                exit(EXIT_SUCCESS);
		        write(sockfd, texte, MAX);
		    }
		    else{
		    }
		}else{
		    write(sockfd, texte, MAX);
		}
		write(sockfd, texte, MAX);
		bzero(texte, MAX);
		read(sockfd, texte, MAX);
		printf("From Server : %s", texte);
		
	}

	// close the socket
	close(sockfd);
}

char* structToString(connexion co){
    char* s=strcat(co.requetes,",");
    char* s2=strcat(s,co.pseudo);
    char* s3=strcat(s2,",");
    char* s4=strcat(s3,co.mdp);
    return s4;
}


int main(int argc, char* argv[]){
    printf("pour faire une requête taper : \n\">inscription\"\n\">connexion\"  \n\">deconnexion\" \n\">liste\" \n\">desinscription \nsinon écrivez un message");
    while(1){communiquer();}

}

